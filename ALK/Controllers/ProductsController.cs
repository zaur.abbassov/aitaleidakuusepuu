﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ALK.Models;

namespace ALK.Controllers
{
    public class ProductsController : Controller
    {
        private UsedGamesEntities db = new UsedGamesEntities();
        //[OutputCache(VaryByParam = "*", Location = System.Web.UI.OutputCacheLocation.ServerAndClient, Duration = 3600)]

        // GET: Products
        [Authorize]
        public ActionResult Index()
        {
            ApplicationUser auser = ApplicationUser.GetUSer(User.Identity.Name);
            if (auser == null)
            {
                return HttpNotFound();
            }

            var products = db.Products.Where(x => x.AspNetUser.DisplayName == auser.DisplayName);
            products= products.Where(x => x.DefaultImage == null);
            return View(products.Select(x => new ProductX { Product = x, FirstImageId = x.ProductImages.Select(y => y.ImageId).FirstOrDefault() }).ToList());
        }


        ////lisan LIKe Conbtrolleri rida 33-67
        //public ActionResult Like(int id)
        //{
        //    AspNetUser u = db.AspNetUsers.Where(x => x.Email == User.Identity.Name).SingleOrDefault();
        //    Product p = db.Products.Find(id);
        //    if (u != null && p != null)
        //    {
        //        try
        //        {
        //            p.Likers.Add(u);
        //            db.SaveChanges();
        //        }
        //        catch (Exception)
        //        {
        //        }
        //    }
        //    return RedirectToAction("Index");
        //}
        //public ActionResult DisLike(int id)
        //{
        //    AspNetUser u = db.AspNetUsers.Where(x => x.Email == User.Identity.Name).SingleOrDefault();
        //    Product p = db.Products.Find(id);
        //    if (u != null && p != null)
        //    {
        //        try
        //        {
        //            p.Likers.Remove(u);
        //            db.SaveChanges();
        //        }
        //        catch (Exception)
        //        {
        //        }
        //    }
        //    return RedirectToAction("Index");
        //}



        // GET: Products/Details/5
        public ActionResult Details(int? id)
        {

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = db.Products.Find(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }

        // GET: Products/Create
        [Authorize]
        public ActionResult Create()
        {
            ApplicationUser auser = ApplicationUser.GetUSer(User.Identity.Name);
            if (auser == null)
            {
                return HttpNotFound();
            }

            ViewBag.UserId = new SelectList(db.AspNetUsers.Where(x => x.DisplayName == auser.DisplayName), "Id", "Email");
            ViewBag.CategoryId = new SelectList(db.Categories.Where(x=>x.Title != "Kõik kategooriad"), "Id", "Title");
            return View();
        }

        // POST: Products/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,UserId,Available,ProductName,Description,Price,Created,Starting,PCount,CategoryId")] Product product, IEnumerable<HttpPostedFileBase> file)
        {
            if (ModelState.IsValid)
            {
                //if (file.Count() > 0)
                {
                    foreach (var item in file)
                    {
                        if (item == null) break;
                        using (System.IO.BinaryReader br = new System.IO.BinaryReader(item.InputStream))
                        {
                            var df = new DataFile
                            {
                                Content = br.ReadBytes(item.ContentLength),
                                Name = item.FileName.Split('\\').Last().Split('/').Last(),
                                ContentType = item.ContentType
                            };

                            db.DataFiles.Add(df);

                            db.Set<ProductImage>().Add(new ProductImage()
                            {
                                ProductId = product.Id,
                                Description = "Create from controller",
                                DataFile = df
                            });
                        }
                    }
                }

                if (product.ProductName == null) return RedirectToAction("Create");
                product.Created = DateTime.Now;
                product.UserId = ApplicationUser.GetUSer(User.Identity.Name).Id;
                db.Products.Add(product);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.UserId = new SelectList(db.AspNetUsers, "Id", "UserId", product.UserId);
            ViewBag.CategoryId = new SelectList(db.Categories, "Id", "Title", product.CategoryId);
            return View(product);
        }

        // GET: Products/Edit/5
        [Authorize]
        public ActionResult Edit(int? id)
        {
            ApplicationUser auser = ApplicationUser.GetUSer(User.Identity.Name);


            if (auser == null)
            {
                return HttpNotFound();
            }

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = db.Products.Find(id);
            if (product == null)
            {
                return HttpNotFound();
            }

            if (product.UserId != auser.Id) return HttpNotFound();


            ViewBag.ImageId = product.ProductImages.FirstOrDefault()?.ImageId;
            ViewBag.UserId = new SelectList(db.AspNetUsers.Where(x => x.DisplayName == auser.DisplayName), "Id", "DisplayName", product.UserId);
            
            ViewBag.CategoryId = new SelectList(db.Categories.Where(x=>x.Title !="Kõik kategooriad"), "Id", "Title", product.CategoryId);
            ViewBag.ProductID = product.Id;

            if(product.DefaultImage != null) return HttpNotFound();
            return View(product);




        }

        // POST: Products/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,UserId,Available,ProductName,Description,Price,Created,Starting,PCount,CategoryId")] string oldImageId, Product product, HttpPostedFileBase file)
        {

            //int oldImageResolvedId;

            //int.TryParse(oldImageId, out oldImageResolvedId);

            ApplicationUser auser = ApplicationUser.GetUSer(User.Identity.Name);


            if (ModelState.IsValid)
            {
                product.Created = DateTime.Now;
                product.UserId = auser.Id;
                db.Entry(product).State = EntityState.Modified;
               
                
                db.SaveChanges();
                if (file != null && file.ContentLength > 0)
                {
                    using (System.IO.BinaryReader br = new System.IO.BinaryReader(file.InputStream))
                    {
                        var df = new DataFile
                        {
                            Content = br.ReadBytes(file.ContentLength),
                            Name = file.FileName.Split('\\').Last().Split('/').Last(),
                            ContentType = file.ContentType
                        };

                        db.DataFiles.Add(df);

                        //if (oldImageResolvedId != 0)
                        //{
                        //    var oldImage = db.DataFiles.Where(x => x.Id == oldImageResolvedId).FirstOrDefault();
                        //    var targets = db.Set<ProductImage>().Where(x => x.ImageId == oldImageResolvedId).Select(x => x);

                        //    foreach (var target in targets)
                        //    {
                        //        db.Set<ProductImage>().Remove(target);
                        //    }

                        //    db.Set<DataFile>().Remove(oldImage);
                        //}

                      

                        db.Set<ProductImage>().Add(new ProductImage()
                        {
                            ProductId = product.Id,
                            Description = "Create from controller",
                            DataFile = df
                        });

                        db.SaveChanges();
                    }
                }

                return RedirectToAction("Edit/" + product.Id);
            }
            ViewBag.UserId = new SelectList(db.AspNetUsers, "Id", "DisplayName", product.UserId);
            ViewBag.CategoryId = new SelectList(db.Categories, "Id", "Title", product.CategoryId);
            return View(product);
        }

        // GET: Products/Delete/5
        [Authorize]
        public ActionResult Delete(int? id)
        {
            ApplicationUser auser = ApplicationUser.GetUSer(User.Identity.Name);


            if (auser == null)
            {
                return HttpNotFound();
            }

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = db.Products.Find(id);
            if (product.UserId != auser.Id) return HttpNotFound();
            if (product == null)
            {
                return HttpNotFound();
            }

            return View(product);
        }

        // POST: Products/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Product product = db.Products.Find(id);
            product.DefaultImage = 1;
            //var image = db.ProductImages.Where(x => x.ProductId == id);

            //foreach (var item in image)
            //{
            //    var df = db.DataFiles.Find(item.ImageId);
            //    db.ProductImages.Remove(item);
            //    db.DataFiles.Remove(df);
            //}

           // db.Products.Remove(product);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

      
        public ActionResult RemovePicutre(int PicturesId, int ProductId)
        {
            var item1 = db.ProductImages.Where(x => x.ImageId == PicturesId).ToList();
            foreach (var item in item1)
            {
                db.ProductImages.Remove(item);
                var item2 = db.DataFiles.Find(item.ImageId);
                db.DataFiles.Remove(item2);
            }
            db.SaveChanges();

            return RedirectToAction("Edit/" + ProductId);
        }
    }
}
