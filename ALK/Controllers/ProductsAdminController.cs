﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ALK.Models;


namespace ALK.Controllers
{
    public class ProductsAdminController : Controller
    {
        private UsedGamesEntities db = new UsedGamesEntities();

        // GET: ProductsAdmin
        [Authorize(Roles = "Admin")]
        public ActionResult Index()
        {
            var products = db.Products.Include(p => p.AspNetUser).Include(p => p.Category);

            products = products.Where(x => x.DefaultImage == null);
            return View(products.Select(x => new ProductX { Product = x, FirstImageId = x.ProductImages.Select(y => y.ImageId).FirstOrDefault() }).ToList());
        }

        // GET: ProductsAdmin/Details/5
        [Authorize(Roles = "Admin")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = db.Products.Find(id);
            if (product == null)
            {
                return HttpNotFound();
            }

            ViewBag.ImageId = product.ProductImages.FirstOrDefault()?.ImageId;
            ViewBag.CategoryId = new SelectList(db.Categories.Where(x => x.Title != "Kõik kategooriad"), "Id", "Title", product.CategoryId);
            ViewBag.ProductID = product.Id;

            if (product.DefaultImage != null) return HttpNotFound();
            return View(product);


        }

        // GET: ProductsAdmin/Create
        [Authorize(Roles = "Admin")]
        public ActionResult Create()
        {
            ViewBag.UserId = new SelectList(db.AspNetUsers, "Id", "Email");
            ViewBag.CategoryId = new SelectList(db.Categories, "Id", "Title");
            return View();
        }

        // POST: ProductsAdmin/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,UserId,Available,ProductName,Description,Price,Created,Starting,PCount,CategoryId,DefaultImage")] Product product, IEnumerable<HttpPostedFileBase> file)
        {
            if (ModelState.IsValid)
            {
                {
                    foreach (var item in file)
                    {
                        if (item == null) break;
                        using (System.IO.BinaryReader br = new System.IO.BinaryReader(item.InputStream))
                        {
                            var df = new DataFile
                            {
                                Content = br.ReadBytes(item.ContentLength),
                                Name = item.FileName.Split('\\').Last().Split('/').Last(),
                                ContentType = item.ContentType
                            };

                            db.DataFiles.Add(df);

                            db.Set<ProductImage>().Add(new ProductImage()
                            {
                                ProductId = product.Id,
                                Description = "Create from controller",
                                DataFile = df
                            });
                        }
                    }
                }

                if (product.ProductName == null) return RedirectToAction("Create");
                product.Created = DateTime.Now;
                db.Products.Add(product);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.UserId = new SelectList(db.AspNetUsers, "Id", "Email", product.UserId);
            ViewBag.CategoryId = new SelectList(db.Categories, "Id", "Title", product.CategoryId);
            return View(product);
        }

        // GET: ProductsAdmin/Edit/5
        [Authorize(Roles = "Admin")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = db.Products.Find(id);
            if (product == null)
            {
                return HttpNotFound();
            }

            ViewBag.ImageId = product.ProductImages.FirstOrDefault()?.ImageId;
            ViewBag.UserId = new SelectList(db.AspNetUsers, "Id", "Email", product.UserId);
            ViewBag.CategoryId = new SelectList(db.Categories, "Id", "Title", product.CategoryId);
            ViewBag.ProductID = product.Id;

            if (product.DefaultImage != null) return HttpNotFound();
            return View(product);
        }

        // POST: ProductsAdmin/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,UserId,Available,ProductName,Description,Price,Created,Starting,PCount,CategoryId,DefaultImage")] string oldImageId, Product product, HttpPostedFileBase file)
        {
         if (ModelState.IsValid)
            {
                product.Created = DateTime.Now;
                db.Entry(product).State = EntityState.Modified;
               
                
                db.SaveChanges();
                if (file != null && file.ContentLength > 0)
                {
                    using (System.IO.BinaryReader br = new System.IO.BinaryReader(file.InputStream))
                    {
                        var df = new DataFile
                        {
                            Content = br.ReadBytes(file.ContentLength),
                            Name = file.FileName.Split('\\').Last().Split('/').Last(),
                            ContentType = file.ContentType
                        };

                        db.DataFiles.Add(df);
                        db.Set<ProductImage>().Add(new ProductImage()
                        {
                            ProductId = product.Id,
                            Description = "Create from controller",
                            DataFile = df
                        });

                        db.SaveChanges();
                    }
                }

                return RedirectToAction("Edit/" + product.Id);
            }
             ViewBag.UserId = new SelectList(db.AspNetUsers, "Id", "Email", product.UserId);
             ViewBag.CategoryId = new SelectList(db.Categories, "Id", "Title", product.CategoryId);
            return View(product);
        }

        // GET: ProductsAdmin/Delete/5
        [Authorize(Roles = "Admin")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = db.Products.Find(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }

        // POST: ProductsAdmin/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Product product = db.Products.Find(id);
            product.DefaultImage = 1;
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public ActionResult RemovePicutre(int PicturesId, int ProductId)
        {
            var item1 = db.ProductImages.Where(x => x.ImageId == PicturesId).ToList();
            foreach (var item in item1)
            {
                db.ProductImages.Remove(item);
                var item2 = db.DataFiles.Find(item.ImageId);
                db.DataFiles.Remove(item2);
            }
            db.SaveChanges();

            return RedirectToAction("Edit/" + ProductId);
        }
    }
}