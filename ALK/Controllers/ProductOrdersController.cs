﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ALK.Models;

namespace ALK.Controllers
{
    public class ProductOrdersController : Controller
    {
        private UsedGamesEntities db = new UsedGamesEntities();
        //[OutputCache(VaryByParam = "*", Location = System.Web.UI.OutputCacheLocation.ServerAndClient, Duration = 3600)]

        // GET: ProductOrders
        [Authorize]
        public ActionResult Index(int? id)
        {


            var auser = ApplicationUser.GetUSer(User.Identity.Name);
            var productOrders = db.ProductOrders
                .Where(x => x.UserId == auser.Id).Where(x => x.PaymentDone == false);

            var s = DateTime.Now.ToString("dd:MM:yyyy:HH:mm:ss");
            if (id == 1)
            {
                foreach (var po in productOrders.ToList())
                {
                    var z = db.ProductOrders.Find(po.Id);
                    z.PaymentDone = true;

                    s = s.Replace(":", "");
                    z.OrderNumber = auser.Id.Substring(0, 4) + s;
                    db.Entry(z).State = EntityState.Modified;
                    db.SaveChanges();
                }
            }

            var itemAmount = db.ProductOrders.Where(x => x.UserId == auser.Id).Where(x => x.PaymentDone == false).Select(x => x.PaymentAmount).ToList();
            var itemQty = db.ProductOrders.Where(x => x.UserId == auser.Id).Where(x => x.PaymentDone == false).Select(x => x.Qty).ToList();
            decimal? itemTotal = 0;

            for (int i = 0; i < itemAmount.Count(); i++)
            {
                itemTotal = itemTotal + itemAmount[i] * itemQty[i];
            }

            if (itemTotal != 0)
            {
                ViewBag.TotalAmount = itemTotal;
            }
            //ViewBag.TotalAmount = db.ProductOrders.Where(x => x.UserId == auser.Id).Where(x => x.PaymentDone == false).Select(x => x.PaymentAmount).Sum();
            // ViewBag.TotalAmount = itemTotal;
            //productOrders.Where(x => x.Delivered == false);
            return View(productOrders.ToList());
        }

        [Authorize]
        public ActionResult Inde_x()
        {
            var auser = ApplicationUser.GetUSer(User.Identity.Name);
            var item = db.ProductOrders.Where(x => x.Product.UserId == auser.Id)
                .Where(x => x.PaymentDone)
                .Where(x => x.Delivered == false);

            return View(item.ToList());
        }

        [Authorize]
        public ActionResult Send([Bind(Include = "Id,ProductId,UserId,Qty,OrderDate,Description,Delivered,DeliveryDate,DeliveryAddress,PaymentDone,PaymentDate,PaymentAmount")] ProductOrder productOrder, string OrderNumbers)
        {
            var auser = ApplicationUser.GetUSer(User.Identity.Name);
            var item = db.ProductOrders.Where(x => x.Product.UserId == auser.Id).Where(x => x.OrderNumber == OrderNumbers).ToList();
            foreach (var it in item)
            {
                var z = db.ProductOrders.Find(it.Id);
                z.Delivered = true;
                db.Entry(z).State = EntityState.Modified;
            }
            db.SaveChanges();

            return RedirectToAction("Inde_x");
        }

        // GET: ProductOrders/Details/5
        public ActionResult Details(int? id)
        {
            //ViewBag.DetailsID = id;
            return RedirectToAction("Details", "ProductList", new { id = id });
        }

        // GET: ProductOrders/Create
        public ActionResult Create()

        {
            ViewBag.UserId = new SelectList(db.AspNetUsers, "Id", "Email");
            ViewBag.ProductId = new SelectList(db.Products, "Id", "UserId");
            return View();
        }

        // POST: ProductOrders/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,ProductId,UserId,Qty,OrderDate,Description,Delivered,DeliveryDate,DeliveryAddress,PaymentDone,PaymentDate,PaymentAmount")] ProductOrder productOrder)
        {
            if (ModelState.IsValid)
            {
                db.ProductOrders.Add(productOrder);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.UserId = new SelectList(db.AspNetUsers, "Id", "Email", productOrder.UserId);
            ViewBag.ProductId = new SelectList(db.Products, "Id", "UserId", productOrder.ProductId);
            return View(productOrder);
        }

        // GET: ProductOrders/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProductOrder productOrder = db.ProductOrders.Find(id);
            if (productOrder == null)
            {
                return HttpNotFound();
            }
            ViewBag.UserId = new SelectList(db.AspNetUsers, "Id", "Email", productOrder.UserId);
            ViewBag.ProductId = new SelectList(db.Products, "Id", "UserId", productOrder.ProductId);
            return View(productOrder);
        }

        // POST: ProductOrders/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,ProductId,UserId,Qty,OrderDate,Description,Delivered,DeliveryDate,DeliveryAddress,PaymentDone,PaymentDate,PaymentAmount")] ProductOrder productOrder)
        {
            if (ModelState.IsValid)
            {
                db.Entry(productOrder).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.UserId = new SelectList(db.AspNetUsers, "Id", "Email", productOrder.UserId);
            ViewBag.ProductId = new SelectList(db.Products, "Id", "UserId", productOrder.ProductId);
            return View(productOrder);
        }

        // GET: ProductOrders/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProductOrder productOrder = db.ProductOrders.Find(id);
            if (productOrder == null)
            {
                return HttpNotFound();
            }
            return View(productOrder);
        }

        // POST: ProductOrders/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ProductOrder productOrder = db.ProductOrders.Find(id);
            db.ProductOrders.Remove(productOrder);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        [Authorize]
        public ActionResult History()
        {
            var auser = ApplicationUser.GetUSer(User.Identity.Name);
            var por = db.ProductOrders.Where(x => x.Product.UserId == auser.Id)
                .Where(x => x.PaymentDone)
                .Where(x => x.Delivered == true);


            var item1 = db.ProductOrders.Where(x=>x.Delivered ==true).Where(x => x.Product.UserId == auser.Id).Select(x => x.OrderNumber);
            var item2 = db.ProductOrders.Where(x => x.UserId == auser.Id).Select(x=>x.OrderNumber).ToList();

            string Temp = "";
            foreach (var item in item1)
            {
                if (item != null)
                {
                    if (!Temp.Contains(item.ToString()))
                    {
                        Temp = Temp + item.ToString() + ";";
                    }
                }
            }
            if (Temp.Length > 1) 
            Temp = Temp.Substring(0, Temp.Length - 1);
            var Temps1 = Temp.Split(';');

            ViewBag.SellsTitle = Temps1;

            
                Temp = "";
            foreach (var item in item2)
            {
                if (item != null)
                {
                    if (!Temp.Contains(item.ToString()))
                    {
                        Temp = Temp + item.ToString() + ";";
                    }
                }
            }
            if (Temp.Length > 1)
                Temp = Temp.Substring(0, Temp.Length - 1);
            var Temps2 = Temp.Split(';');

            ViewBag.BoyTitle = Temps2;

            return View(por.ToList());
        }
    }
}
