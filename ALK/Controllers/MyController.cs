﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ALK.Models;

namespace ALK.Models
{
    public class DataFilex
    { 
        public int Id { get; set; }
        public string Name { get; set; }
        public string ContentType { get; set; }
        public int UsedCount { get; set; }
    }
}

namespace ALK.Controllers
{
    public class MyController : Controller
    {
        static string Logi = "";
        protected UsedGamesEntities db = new UsedGamesEntities();

        [OutputCache(VaryByParam ="*", Location = System.Web.UI.OutputCacheLocation.ServerAndClient, Duration =3600)]

        public ActionResult Content(int id)
        {
            DateTime s = DateTime.Now;
            DataFile df = db.DataFiles.Find(id);
            if (df == null) return HttpNotFound();
            Logi += $"Laadisin {id} - {(DateTime.Now-s).TotalMilliseconds}\n";
            return File(df.Content, df.ContentType);

        }

        public string GetLogi() => Logi;

        [HttpPost]
        public ActionResult Index(HttpPostedFileBase file)
        {
            if (file != null)
            {
                using (System.IO.BinaryReader br = new System.IO.BinaryReader(file.InputStream))
                {
                    db.DataFiles.Add(
                        new DataFile
                        {
                            Name = file.FileName.Split('\\').Last(),
                            ContentType = file.ContentType,
                            Content = br.ReadBytes(file.ContentLength)
                        }
                        );
                    db.SaveChanges();
                }
            }

            return RedirectToAction("Index");
        }

        public ActionResult Index()
        {
            return View(db.DataFiles.Select(x => new DataFilex {Id =x.Id, Name =x.Name, ContentType = x.ContentType, UsedCount = x.ProductImages.Count } ).ToList());
        }


    }
}