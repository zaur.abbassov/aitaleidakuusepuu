﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ALK.Models;
using System.ComponentModel.DataAnnotations;

namespace ALK.Controllers
{
    // Henn (5-7 Turvalised atribuudid.txt) kuidas teha väljund pilti kuupäevale ilusamaks
    //[MetaDataType(typeof(EmployeeMetadata))]
    //public class Employee
    //{
    //    public string Fullname => $"{FirstName} {LastName}";
    //}

    //public class EmployeeMetadata
    //{
    //    [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd.MM.yyyy}")]
    //    public DateTime? BirthDate { get; set; }

    //    [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd.MM.yyyy}")]
    //    public DateTime? HireDate { get; set; }
    //}


    [Authorize(Roles = "Admin")]
    public class AspNetUsersController : Controller
    {
        private UsedGamesEntities db = new UsedGamesEntities();

        // GET: AspNetUsers
        public ActionResult Index()
        {
            var aspNetUsers = db.AspNetUsers.Include(a => a.DataFile);
            return View(aspNetUsers.ToList());
        }

        // GET: AspNetUsers/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AspNetUser aspNetUser = db.AspNetUsers.Find(id);
            if (aspNetUser == null)
            {
                return HttpNotFound();
            }
            
            ViewBag.Roles = db.AspNetRoles.ToList();
            return View(aspNetUser);
        }

        // GET: AspNetUsers/Create
        public ActionResult Create()
        {
            ViewBag.PictureId = new SelectList(db.DataFiles, "Id", "Name");
            return View();
        }

        // POST: AspNetUsers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Email,EmailConfirmed,PasswordHash,SecurityStamp,PhoneNumber,PhoneNumberConfirmed,TwoFactorEnabled,LockoutEndDateUtc,LockoutEnabled,AccessFailedCount,UserName,FirstName,LastName,BirthDate,Phone,Address,PictureId,DisplayName")] AspNetUser aspNetUser)
        {
            if (ModelState.IsValid)
            {
                db.AspNetUsers.Add(aspNetUser);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.PictureId = new SelectList(db.DataFiles, "Id", "Name", aspNetUser.PictureId);
            return View(aspNetUser);
        }

        // GET: AspNetUsers/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AspNetUser aspNetUser = db.AspNetUsers.Find(id);
            if (aspNetUser == null)
            {
                return HttpNotFound();
            }
            ViewBag.PictureId = new SelectList(db.DataFiles, "Id", "Name", aspNetUser.PictureId);
            return View(aspNetUser);
        }

        // POST: AspNetUsers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Email,EmailConfirmed,PasswordHash,SecurityStamp,PhoneNumber,PhoneNumberConfirmed,TwoFactorEnabled,LockoutEndDateUtc,LockoutEnabled,AccessFailedCount,UserName,FirstName,LastName,BirthDate,Phone,Address,PictureId,DisplayName")] AspNetUser aspNetUser)
        {
            if (ModelState.IsValid)
            {
                db.Entry(aspNetUser).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.PictureId = new SelectList(db.DataFiles, "Id", "Name", aspNetUser.PictureId);
            return View(aspNetUser);
        }

        // GET: AspNetUsers/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AspNetUser aspNetUser = db.AspNetUsers.Find(id);
            if (aspNetUser == null)
            {
                return HttpNotFound();
            }
            return View(aspNetUser);
        }

        // POST: AspNetUsers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            AspNetUser aspNetUser = db.AspNetUsers.Find(id);
            db.AspNetUsers.Remove(aspNetUser);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        [Authorize(Roles = "Admin")] //(Roles = "Admin") need sulgudega tekst panna [Autorize(Roles = "Admin")] või siis [Authorize(Users = "henn@sarv.ee")]
        public ActionResult AddRole(string id, string roleId)
        {
            AspNetUser user = db.AspNetUsers.Find(id);
            AspNetRole role = db.AspNetRoles.Find(roleId);

            if (user != null && role != null)
           

            {
                try
                {
                    user.AspNetRoles.Add(role);
                    db.SaveChanges();

                }
                    catch (Exception)
                {
                }

            }
            
            return RedirectToAction("Details", new { id });
        }

        [Authorize(Roles = "Admin")]
        public ActionResult RemoveRole(string id, string roleId)
        {
            AspNetUser user = db.AspNetUsers.Find(id);
            AspNetRole role = db.AspNetRoles.Find(roleId);

            if (user != null && role != null)
                if (user.Email != User.Identity.Name || role.Name != "Admin") // et adminisraator kogemata ei saaks ise endal ära võtta administraatori rolli

                //if (user.Email != User.Identity.Name || role.Name != "Admin")  // juhul kui tegemist on rolliga kui kasutajate on rohkem kui vähem, kontrollin et adminn ei saks viimast adminni ära kustutada
                if (role.Name != "Admin" || role.AspNetUsers.Count() > 1)
                {
                    try
                    {
                        user.AspNetRoles.Remove(role);
                        db.SaveChanges();
                    }

                    catch (Exception)
                    {

                    }
                }

            return RedirectToAction("Details", new { id });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
