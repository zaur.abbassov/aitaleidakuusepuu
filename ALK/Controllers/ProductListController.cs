﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ALK.Models;
namespace ALK.Models
{
    public class ProductX
    {
        public Product Product { get; set; }
        public int? FirstImageId { get; set; }
    }
}

namespace ALK.Controllers
{
    public class ProductListController : Controller
    {
        private UsedGamesEntities db = new UsedGamesEntities();
        // [OutputCache(VaryByParam = "*", Location = System.Web.UI.OutputCacheLocation.ServerAndClient, Duration = 3600)]

        // GET: ProductList
        public ActionResult Index(int? id)
        {
            if (id == 4)
            {
                id = null;
            }
            var products = db.Products.Where(x => x.Available)
                .Where(x => !id.HasValue || x.CategoryId == id.Value);
            ViewBag.CategoryName = id.HasValue
                ? db.Categories.Find(id.Value)?.Title ?? ""
                : "Kõik tooted";

            products = products.Where(x => x.DefaultImage == null);
            if (id != null) ViewBag.CategoryId = id.Value;
            return View(products.Select(x => new ProductX { Product = x, FirstImageId = x.ProductImages.Select(y => y.ImageId).FirstOrDefault() }).ToList());
        }

        // GET: ProductList/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = db.Products.Find(id);
            if (product == null)
            {
                return HttpNotFound();
            }

            var item = db.Comments.Where(x => x.ProductId == id).Select(x => x).ToList();
            ViewBag.Comments = item;


            return View(product);
        }

        [Authorize]
        public ActionResult AddComment(int id, string content)
        {
            var auser = ApplicationUser.GetUSer(User.Identity.Name);
            Product p = db.Products.Find(id);
            var item = new Comment()
            {
                ProductId = id,
                Commenter = auser.Id,
                CommentText = content,


            };

            db.Comments.Add(item);
            db.SaveChanges();
            return RedirectToAction("Details/" + id);
        }
        public ActionResult RemoveComment(int id)
        {
            Comment c = db.Comments.Find(id);
            if (c != null)
            {
                db.Comments.Remove(c);
                db.SaveChanges();
            }
            return RedirectToAction("Details");

        }


        // GET: ProductList/Create
        public ActionResult Create()
        {
            ViewBag.UserId = new SelectList(db.AspNetUsers, "Id", "Email");
            ViewBag.CategoryId = new SelectList(db.Categories, "Id", "Title");
            return View();
        }

        // POST: ProductList/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,UserId,Available,ProductName,Description,Price,Created,Starting,PCount,CategoryId,DefaultImage")] Product product)
        {
            if (ModelState.IsValid)
            {
                db.Products.Add(product);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.UserId = new SelectList(db.AspNetUsers, "Id", "Email", product.UserId);
            ViewBag.CategoryId = new SelectList(db.Categories, "Id", "Title", product.CategoryId);
            return View(product);
        }

        // GET: ProductList/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = db.Products.Find(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            ViewBag.UserId = new SelectList(db.AspNetUsers, "Id", "Email", product.UserId);
            ViewBag.CategoryId = new SelectList(db.Categories, "Id", "Title", product.CategoryId);

            if (product.DefaultImage != null) return HttpNotFound();
            return View(product);
        }

        // POST: ProductList/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,UserId,Available,ProductName,Description,Price,Created,Starting,PCount,CategoryId,DefaultImage")] Product product)
        {
            if (ModelState.IsValid)
            {
                db.Entry(product).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.UserId = new SelectList(db.AspNetUsers, "Id", "Email", product.UserId);
            ViewBag.CategoryId = new SelectList(db.Categories, "Id", "Title", product.CategoryId);
            return View(product);
        }

        // GET: ProductList/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = db.Products.Find(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }

        // POST: ProductList/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Product product = db.Products.Find(id);
            db.Products.Remove(product);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        [Authorize]
        public ActionResult Buy([Bind(Include = "Id,ProductId,UserId,Qty,OrderDate,Description,Delivered,DeliveryDate,DeliveryAddress,PaymentDone,PaymentDate,PaymentAmount")] ProductOrder productOrder, decimal Price, string ProductN, int Count, int ProductId)
        {
            var auser = ApplicationUser.GetUSer(User.Identity.Name);
            var item1 = db.ProductOrders.Where(x => x.UserId == auser.Id).ToList();
            item1 = item1.Where(x => x.ProductId == ProductId).ToList();
            var item2 = item1.Where(x => x.PaymentDone == false).ToList();

            if (item2.Count == 0)
            {

                db.ProductOrders.Add(new ProductOrder
                {
                    ProductId = ProductId,
                    UserId = auser.Id,
                    Qty = 1,
                    OrderDate = DateTime.Now,
                    Delivered = false,
                    PaymentDone = false,
                    PaymentAmount = Price
                });
            }
            else
            {
                productOrder = db.ProductOrders.Find(item2.First().Id);
                productOrder.Qty += 1;
                db.Entry(productOrder).State = EntityState.Modified;

            }

            db.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}
