﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace ALK.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit https://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string DisplayName { get; set; }
        public DateTime BirthDate { get; set; }
        public int PictureId { get; set; }



        public static ApplicationUser GetUSer(string username)
        {
            //ApplicationUser user;
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                //user = db.Users.FirstOrDefaultAsync(x => x.Email == username).Result;
                var collection = db.Set<ApplicationUser>().Where(x => x.Email == username).ToList();
                //var collection = db.Users.Where(x => x.Email == username).ToList();
                return collection.FirstOrDefault();
            }
            
        }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
    }

    public class TestApp :ProductImage
    {

    }
}