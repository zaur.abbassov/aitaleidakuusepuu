//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ALK.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class ProductOrder
    {
        public int Id { get; set; }
        public int ProductId { get; set; }
        public string UserId { get; set; }
        public int Qty { get; set; }
        public System.DateTime OrderDate { get; set; }
        public string Description { get; set; }
        public bool Delivered { get; set; }
        public Nullable<System.DateTime> DeliveryDate { get; set; }
        public string DeliveryAddress { get; set; }
        public bool PaymentDone { get; set; }
        public Nullable<System.DateTime> PaymentDate { get; set; }
        public Nullable<decimal> PaymentAmount { get; set; }
        public string OrderNumber { get; set; }
    
        public virtual AspNetUser AspNetUser { get; set; }
        public virtual Product Product { get; set; }
    }
}
