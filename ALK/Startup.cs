﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ALK.Startup))]
namespace ALK
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
